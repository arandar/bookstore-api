import { Collection } from '@typespring/migrator';
import { Collections } from '../models/enums/collections';
import { ObjectId } from 'mongodb';

export const MOCK_USER1_ID = new ObjectId('5c1a3d09e7c2571ee8c138a1');
export const MOCK_USER2_ID = new ObjectId('5c1a3d09e7c2571ee8c138a2');


@Collection({
    name: Collections.Users,
    schema: {
        bsonType: 'object',
        required: ['_id', 'role', 'firstName', 'lastName', 'username', 'email', 'password', 'avatar', 'createdAt'],
        additionalProperties: false,
        properties: {
            _id: {
                bsonType: 'objectId',
                description: 'must be a string and is required',
            },
            role: {
                enum: ['STAFF', 'USER'],
                description: 'can only be one of the enum values and is required',
            },
            firstName: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            lastName: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            username: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            email: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            password: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            avatar: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            createdAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
            updatedAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
        },
    },
    items: [
        {
            _id: MOCK_USER1_ID,
            role: 'STAFF',
            firstName: 'Rostislav',
            lastName: 'Kovalev',
            username: 'Arandar',
            email: 'user1@mail.ru',
            password: '12345',
            avatar: 'assets/images/no-avatar.png',
            createdAt: new Date(),
        },
        {
            _id: MOCK_USER2_ID,
            role: 'USER',
            firstName: 'Yuri',
            lastName: 'Boyko',
            username: 'Иutcher',
            email: 'user2@mail.ru',
            password: '12345',
            avatar: 'assets/images/no-avatar.png',
            createdAt: new Date(),
        },
    ],
})
export class UsersCollection { }