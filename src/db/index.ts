import { Migrator } from '@typespring/migrator';
import * as dotenv from 'dotenv';
import { BooksCollection } from './_books';
import { OrdersCollection } from './_orders';
import { UsersCollection } from './_users';

dotenv.config();

export const MIGRATOR_COLLECTIONS = [
    BooksCollection,
    OrdersCollection,
    UsersCollection,
];

@Migrator({
    mongoUrl: process.env.MONGO_URL,
    collections: MIGRATOR_COLLECTIONS,
})
class AppMigrator {
    // tslint:disable-next-line:no-empty
    run = () => {
    }
}

new AppMigrator().run();