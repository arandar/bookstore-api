import { Collection } from '@typespring/migrator';
import { Collections } from '../models/enums/collections';
import { ObjectId } from 'mongodb';

export const MOCK_ORDER1_ID = new ObjectId('5c1a3d09e7c2571ee8c137a1');

@Collection({
    name: Collections.Orders,
    schema: {
        bsonType: 'object',
        required: ['_id', 'name', 'adress', 'createdAt'],
        additionalProperties: false,
        properties: {
            _id: {
                bsonType: 'objectId',
                description: 'must be a string and is required',
            },
            name: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            adress: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            email: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            phone: {
                bsonType: 'object',
                required: ['code', 'number'],
                additionalProperties: false,
                properties: {
                    code: {
                        bsonType: 'string',
                        description: 'must be a string and is required',
                    },
                    number: {
                        bsonType: 'number',
                        description: 'must be a number and is required',
                    },
                },
            },
            books: {
                bsonType: 'array',
                description: 'must be a array',
            },
            createdAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
            updatedAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
        },
    },
    items: [
        {
            _id: MOCK_ORDER1_ID,
            name: 'Antony Workfield',
            adress: 'New York, Wall Street 7A',
            phone: { code: '+7', number: 1234567 },
            email: 'faa@email.com',
            books: [],
            createdAt: new Date(),
        },
    ],
})
export class OrdersCollection { }