import { Collection } from '@typespring/migrator';
import { Collections } from '../models/enums/collections';
import { ObjectId } from 'mongodb';

export const MOCK_BOOK1_ID = new ObjectId('5c1a3d09e7c2571ee8c149a1');
export const MOCK_BOOK2_ID = new ObjectId('5c1a3d09e7c2571ee8c149a2');
export const MOCK_BOOK3_ID = new ObjectId('5c1a3d09e7c2571ee8c149a3');
export const MOCK_BOOK4_ID = new ObjectId('5c1a3d09e7c2571ee8c149a4');
export const MOCK_BOOK5_ID = new ObjectId('5c1a3d09e7c2571ee8c149a5');
export const MOCK_BOOK6_ID = new ObjectId('5c1a3d09e7c2571ee8c149a6');
export const MOCK_BOOK7_ID = new ObjectId('5c1a3d09e7c2571ee8c149a7');
export const MOCK_BOOK8_ID = new ObjectId('5c1a3d09e7c2571ee8c149a8');
export const MOCK_BOOK9_ID = new ObjectId('5c1a3d09e7c2571ee8c149a9');

@Collection({
    name: Collections.Books,
    schema: {
        bsonType: 'object',
        required: ['_id', 'img', 'author', 'title', 'description', 'price', 'createdAt'],
        additionalProperties: false,
        properties: {
            _id: {
                bsonType: 'objectId',
                description: 'must be a string and is required',
            },
            img: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            author: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            title: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            description: {
                bsonType: 'string',
                description: 'must be a string and is required',
            },
            price: {
                bsonType: 'number',
                description: 'must be a number and is required',
            },
            createdAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
            updatedAt: {
                bsonType: 'date',
                description: 'must be a date',
            },
        },
    },
    items: [
        {
            _id: MOCK_BOOK1_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/41gIrr4WtQL._SX296_BO1,204,203,200_.jpg',
            author: 'Ray Bradbury',
            title: 'Fahrenheit 451',
            description: `Fahrenheit 451 is a dystopian novel by American writer Ray Bradbury,
            first published in 1953. It is regarded as one of his best works. The novel presents
            a future American society where books are outlawed and "firemen" burn any that are
            found. The book's tagline explains the title: "Fahrenheit 451 – the temperature at
            which book paper catches fire, and burns..." The lead character, Guy Montag, is a
            fireman who becomes disillusioned with his role of censoring literature and destroying
            knowledge, eventually quitting his job and committing himself to the preservation of
            literary and cultural writings.`,
            price: 16.99,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK2_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/41f5qHFIlqL._SX307_BO1,204,203,200_.jpg',
            author: 'Nathaniel Hawthorne',
            title: 'The Scarlet Letter',
            description: `The Scarlet Letter is an 1850 romantic work of fiction in a historical
            setting, written by Nathaniel Hawthorne, and is considered to be his magnum opus. Set in
            17th-century Puritan Boston, Massachusetts during the years 1642 to 1649, it tells the story
            of Hester Prynne, who conceives a daughter through an affair and struggles to create a new
            life of repentance and dignity. Throughout the book, Hawthorne explores themes of legalism,
            sin, and guilt.`,
            price: 19.98,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK3_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/51aFNKc2hmL.jpg',
            author: 'Mark Twain',
            title: 'Adventures of Huckleberry Finn',
            description: `Adventures of Huckleberry Finn (or, in more recent editions, The Adventures
                of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in
                December 1884 and in the United States in February 1885. Commonly named among the Great
                American Novels, the work is among the first in major American literature to be written
                throughout in vernacular English, characterized by local color regionalism. It is told in
                the first person by Huckleberry "Huck" Finn, a friend of Tom Sawyer and narrator of two
                other Twain novels (Tom Sawyer Abroad and Tom Sawyer, Detective). It is a direct sequel
                to The Adventures of Tom Sawyer. The book is noted for its colorful description of people
                and places along the Mississippi River. Set in a Southern antebellum society that had ceased
                to exist about twenty years before the work was published, Adventures of Huckleberry Finn is
                an often scathing satire on entrenched attitudes, particularly racism. Perennially popular
                with readers, Adventures of Huckleberry Finn has also been the continued object of study by
                literary critics since its publication. It was criticized upon release because of its coarse
                language and became even more controversial in the 20th century because of its perceived use
                of racial stereotypes and because of its frequent use of the racial slur "nigger", despite
                strong arguments that the protagonist and the tenor of the book are anti-racist.`,
            price: 17.99,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK4_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/61wfUmGRZzL._SX329_BO1,204,203,200_.jpg',
            author: 'Chuck Palahniuk',
            title: 'Fight Club',
            description: `In this darkly comic drama, Edward Norton stars as a depressed young man (named
                in the credits only as "Narrator") who has become a small cog in the world of big business.
                He doesn't like his work and gets no sense of reward from it, attempting instead to drown
                his sorrows by putting together the "perfect" apartment. He can't sleep and feels alienated
                from the world at large; he's become so desperate to relate to others that he's taken to
                visiting support groups for patients with terminal diseases so that he'll have people to
                talk to. One day on a business flight, he discovers Tyler Durden (Brad Pitt), a charming
                iconoclast who sells soap. Tyler doesn't put much stock in the materialistic world, and
                he believes that one can learn a great deal through pain, misfortune, and chaos. Tyler
                cheerfully challenges his new friend to a fight. Our Narrator finds that bare-knuckle
                brawling makes him feel more alive than he has in years, and soon the two become friends
                and roommates, meeting informally to fight once a week. As more men join in, the "fight club"
                becomes an underground sensation, even though it's a closely guarded secret among the
                participants. (First rule: Don't talk about fight club. Second rule: Don't talk about
                fight club.) But as our Narrator and Tyler bond through violence, a strange situation
                becomes more complicated when Tyler becomes involved with Marla (Helena Bonham Carter), whom
                our Narrator became infatuated with when they were both crashing the support-group circuit.
                Based on the novel by Chuck Palahniuk, Fight Club was directed by David Fincher, who previously
                directed Pitt in the thriller Seven. ~ Mark Deming, Rovi`,
            price: 25,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK5_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/41%2BWrsIartL._SX329_BO1,204,203,200_.jpg',
            author: 'Jackie Chan',
            title: 'Never Grow Up',
            description: `Everyone knows Jackie Chan. Whether it’s from Rush Hour, Shanghai Noon, The Karate Kid, 
            or Kung Fu Panda, Jackie is admired by generations of moviegoers for his acrobatic fighting style, 
            comic timing, and mind-bending stunts. In 2016—after fifty-six years in the industry, over 200 films, 
            and many broken bones—he received an honorary Academy Award for his lifetime achievement in film. 
            But at 64 years-old, Jackie is just getting started.
            Now, in Never Grow Up, the global superstar reflects on his early life, including his childhood 
            years at the China Drama Academy (in which he was enrolled at the age of six), his big breaks 
            (and setbacks) in Hong Kong and Hollywood, his numerous brushes with death (both on and off film sets), 
            and his life as a husband and father (which has been, admittedly and regrettably, imperfect).
            Jackie has never shied away from his mistakes. Since The Young Master in 1980, Jackie’s films have 
            ended with a bloopers reel in which he stumbles over his lines, misses his mark, or crashes to the 
            ground in a stunt gone south. In Never Grow Up, Jackie applies the same spirit of openness to his life, 
            proving time and time again why he’s beloved the world over: he’s honest, funny, kind, brave beyond 
            reckoning and—after all this time—still young at heart.`,
            price: 17.10,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK6_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/51rfjMnlwAL._SX342_.jpg',
            author: 'Alexandre Dumas',
            title: 'The Count of Monte Cristo',
            description: `With an Introduction and Notes by Keith Wren, University of Kent at Canterbury The story 
            of Edmund Dantes, self-styled Count of Monte Cristo, is told with consummate skill. The victim of a 
            miscarriage of justice, Dantes is fired by a desire for retribution and empowered by a stroke of providence. 
            In his campaign of vengeance, he becomes an anonymous agent of fate. The sensational narrative of intrigue, 
            betrayal, escape, and triumphant revenge moves at a cracking pace. Dumas' novel presents a powerful conflict 
            between good and evil embodied in an epic saga of rich diversity that is complicated by the hero's ultimate 
            discomfort with the hubristic implication of his own actions.`,
            price: 19.57,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK7_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/519V0IlKZpL._SY346_.jpg',
            author: 'J. R. R. Tolkien',
            title: 'The Hobbit',
            description: `Read the definitive edition of Bilbo Baggins’ adventures in middle-earth in this classic bestseller 
            behind this year’s biggest movie.
            The Hobbit is a tale of high adventure, undertaken by a company of dwarves in search of dragon-guarded gold. 
            A reluctant partner in this perilous quest is Bilbo Baggins, a comfort-loving unambitious hobbit, who surprises 
            even himself by his resourcefulness and skill as a burglar.
            Encounters with trolls, goblins, dwarves, elves and giant spiders, conversations with the dragon, Smaug, and a 
            rather unwilling presence at the Battle of Five Armies are just some of the adventures that befall Bilbo.
            Bilbo Baggins has taken his place among the ranks of the immortals of children’s fiction. Written by Professor 
            Tolkien for his own children, The Hobbit met with instant critical acclaim when published.`,
            price: 10.63,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK8_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/4186P0mACWL._SX336_BO1,204,203,200_.jpg',
            author: 'Antoine de Saint-Exupéry',
            title: 'The Little Prince',
            description: `Few stories are as widely read and as universally cherished by children and adults alike as The Little 
            Prince. Richard Howard's translation of the beloved classic beautifully reflects Saint-Exupéry's unique and gifted 
            style. Howard, an acclaimed poet and one of the preeminent translators of our time, has excelled in bringing the 
            English text as close as possible to the French, in language, style, and most important, spirit. The artwork in this 
            edition has been restored to match in detail and in color Saint-Exupéry's original artwork. Combining Richard Howard's 
            translation with restored original art, this definitive English-language edition of The Little Prince will capture the 
            hearts of readers of all ages.
            This title has been selected as a Common Core Text Exemplar (Grades 4-5, Stories).`,
            price: 12.99,
            createdAt: new Date(),
        },
        {
            _id: MOCK_BOOK9_ID,
            img: 'https://images-na.ssl-images-amazon.com/images/I/51K6iSgqUqL._SX329_BO1,204,203,200_.jpg',
            author: 'C.S. Lewis & Pauline Baynes',
            title: 'The Chronicles of Narnia',
            description: `n impressive hardcover volume containing all seven books in the classic fantasy series The Chronicles of 
            Narnia, graced by black-and-white chapter opening illustrations and featuring an essay by C. S. Lewis on writing. This 
            volume also contains C. S. Lewis's essay "On Three Ways of Writing for Children."
            Fantastic creatures, heroic deeds, epic battles in the war between good and evil, and unforgettable adventures come together
            in this world where magic meets reality, which has been enchanting readers of all ages for over sixty years. The Chronicles 
            of Narnia has transcended the fantasy genre to become a part of the canon of classic literature.
        
            This edition presents all seven books—The Magician's Nephew; The Lion, the Witch and the Wardrobe; The Horse and His Boy; 
            Prince Caspian; The Voyage of the Dawn Treader; The Silver Chair; and The Last Battle—unabridged. The books appear according to 
            C. S. Lewis's preferred order and each chapter features a chapter opening illustration by the original artist, Pauline Baynes.`,
            price: 21.99,
            createdAt: new Date(),
        },
    ],
})
export class BooksCollection { }