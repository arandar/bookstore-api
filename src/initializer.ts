import { Injectable, IServerInitializer, MongoService } from '@typespring/core';
import { ConfigService } from './services/config.service';
import { SocketService } from './services/socket.service';
import { MailerService } from './services/mailer.service';

@Injectable()
export class AppInitializer implements IServerInitializer {
    constructor(
        private config: ConfigService,
        private mongo: MongoService,
        private socketService: SocketService,
        private mailer: MailerService,
    ) {
    }

    init(): void {
        this.mongo.connect({
            url: this.config.mongo.url,
        }).then(() => {
            this.socketService.init();
            this.mailer.init();
        });
    }
}
