import { BooksController } from './controllers/books';
import { OrdersController } from './controllers/orders';
import { OrdersRepository } from './repository/orders';
import { BooksRepository } from './repository/books';
import { UploaderController } from './controllers/uploader';
import { ImageCleanerService } from './services/image-cleaner.service';
import { AuthorizationController } from './controllers/authorization';
// import { AuthorizationRepository } from './repository/authorization';
import { UsersController } from './controllers/users';
import { UsersRepository } from './repository/users';
import { AppInitializer } from './initializer';
import { PUBLIC_DIR_PATH, STATIC_PATH, ROOT } from './models/const/paths';
import { Server, bootstrap, MongoService } from '@typespring/core';
import { ConfigService } from './services/config.service';
import { MailerService } from './services/mailer.service';
import { SocketService } from './services/socket.service';
import { JWTService } from './services/jwt-service';
import { AccessGuardService } from './services/access-guard.service';

@Server({
    port: process.env.API_PORT,
    initializer: AppInitializer,
    publicDir: PUBLIC_DIR_PATH,
    staticDir: STATIC_PATH,
    providers: [
        ConfigService,
        MongoService,
        MailerService,
        SocketService,
        JWTService,
        AccessGuardService,
        ImageCleanerService,
        BooksRepository,
        OrdersRepository,
        UsersRepository,
        // AuthorizationRepository,
    ],
    controllers: [
        AuthorizationController,
        BooksController,
        OrdersController,
        UploaderController,
        UsersController,
    ],
    loggerConfig: {
        console: {
            level: process.env.LOG_CONSOLE_LEVEL,
        },
        file: {
            name: `${ROOT}/error.log`,
            level: process.env.LOG_FILE_LEVEL,
        },
    },
})
class AppServer { }
bootstrap(AppServer);