import { Collections } from './../models/enums/collections';
import { IUser } from '../models/interfaces/user';
import { MailerService } from '../services/mailer.service';
import { ICredentials } from '../models/interfaces/credentials';
import { Injectable, MongoService } from '@typespring/core';
import { ObjectId } from 'mongodb';

@Injectable()
export class UsersRepository {
    constructor(
        private mongo: MongoService,
        private mailerService: MailerService,
    ) { }

    async getUsers(): Promise<IUser[]> {
        return await this.mongo.collection(Collections.Users).find({}).toArray();
    }

    async getUserById(id: string): Promise<IUser> {
        return await this.mongo.collection(Collections.Users).findOne({ _id: new ObjectId(id) });
    }

    async getUserByCredentials(cred: ICredentials): Promise<IUser> {
        const match = await this.mongo.collection(Collections.Users).find({
            email: cred.email,
            password: cred.password,
        }).toArray();
        if (!match || !match.length) return Promise.resolve(undefined);
        return Promise.resolve(match[0]);
    }

    // async editUser(user: IUser): Promise<void> {
    //     const id = user._id;
    //     delete user._id;
    //     await this.mongo.collection(Collections.Users).updateOne({ _id: new ObjectId(id) },
    //         {
    //             $set: {
    //                 firstName: user.firstName,
    //                 lastName: user.lastName,
    //                 username: user.username,
    //                 email: user.email,
    //                 phone: user.phone,
    //                 adress: user.adress,
    //                 avatar: user.avatar,
    //             },
    //         });
    // }

    // async setNewPassword(body: { id: string, newPassword: string }): Promise<void> {
    //     await this.mongo.collection(Collections.Users).findOneAndUpdate({ _id: new ObjectId(body.id) },
    //         { $set: { password: body.newPassword } });
    // }

    // async forgotPassword(email: string): Promise<void> {
    //     const user = await this.mongo.collection(Collections.Users).findOne({ email: email });
    //     this.mailerService.resetPassword(user);
    // }

    // async saveBooks(id, books): Promise<void> {
    //     await this.mongo.collection(Collections.Users).findOneAndUpdate({ _id: new ObjectId(id) },
    //         { $set: { books: books } });
    // }
}