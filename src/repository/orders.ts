import { Collections } from './../models/enums/collections';
import { ObjectId } from 'mongodb';
import { IOrder } from '../models/interfaces/order';
import { Injectable, MongoService } from '@typespring/core';

@Injectable()
export class OrdersRepository {
    constructor(
        private mongo: MongoService,
    ) { }

    getOrders(): Promise<IOrder[]> {
        return this.mongo.collection(Collections.Orders).find({}).toArray();
    }

    async addOrder(order: IOrder): Promise<IOrder> {
        await this.mongo.collection(Collections.Orders).insertOne(order);
        return order;
    }

    async deleteOrder(orderId): Promise<void> {
        const id = new ObjectId(orderId);
        await this.mongo.collection(Collections.Orders).deleteOne({ _id: id });
    }
}