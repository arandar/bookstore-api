import { ObjectId } from 'mongodb';
import { Collections } from './../models/enums/collections';
import { IBook } from '../models/interfaces/book';
import { Injectable, MongoService } from '@typespring/core';

@Injectable()
export class BooksRepository {
    constructor(
        private mongo: MongoService,
    ) { }

    async getBooks(index, size, search): Promise<{ books: IBook[], booksCount: number }> {
        await this.mongo.collection(Collections.Books).createIndex({ author: 'text', title: 'text' });
        const find = this.mongo.collection(Collections.Books)
            .find({
                $or: [
                    { author: { $regex: search, $options: 'i' } },
                    { title: { $regex: search, $options: 'i' } },
                ],
            });
        const books = await find.skip(index * size).limit(size).toArray();
        const filterCount = await find.count();
        return {
            books: books,
            booksCount: filterCount,
        };
    }

    async addBook(book: IBook): Promise<IBook> {
        await this.mongo.collection(Collections.Books).insertOne(book);
        return book;
    }

    async updateBook(book): Promise<void> {
        const id = book._id;
        delete book._id;
        await this.mongo.collection(Collections.Books).replaceOne({ _id: new ObjectId(id) }, book);
    }

    async deleteBook(bookId): Promise<void> {
        const id = new ObjectId(bookId);
        await this.mongo.collection(Collections.Books).deleteOne({ _id: id });
    }
}