// import { Collections } from '../models/enums/collections';
// import { Repository, MongoService } from 'typespring';
// import { IUser } from '../models/interfaces/user';
// import { ObjectId } from 'mongodb';

// @Injectable()
// export class AuthorizationRepository {
//     constructor(
//         private mongo: MongoService,
//     ) { }

//     async regUser(user: IUser): Promise<IUser> {
//         await this.mongo.collection(Collections.Users).insertOne(user);
//         return user;
//     }

//     async confirmRegistration(id): Promise<any> {
//         console.log(id);
//         return await this.mongo.collection(Collections.Users)
//             .updateOne({ _id: new ObjectId(id) }, { $set: { active: true } });
//     }

// }