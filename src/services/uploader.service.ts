import { Injectable, Logger } from '@typespring/core';
import { API_ERROR } from '../models/const/errors';
import { slugify } from 'transliteration';
import * as path from 'path';
import * as fs from 'fs';
import * as sharp from 'sharp';
import * as uuid from 'uuid';
import * as winston from 'winston';
import { PUBLIC_DIR_PATH, PRIVATE_DIR_PATH } from '../models/const/paths';

slugify.config({ separator: '_', ignore: ['.'] });

@Injectable()
export class UploaderService {
    @Logger('UPLOADER_SERVICE')
    private logger: winston.Logger;

    async uploadPublic(file: any): Promise<string> {
        if (!file) return Promise.reject(API_ERROR.BAD_REQUEST);
        const filePath = PUBLIC_DIR_PATH;

        if (!fs.existsSync(filePath)) fs.mkdirSync(filePath);

        const id = uuid.v1();
        const fileName = slugify(`${id}_${file.name}`);
        try {
            // await file.mv(`${filePath}/${fileName}`);
            const img = sharp(file.data);
            const metadata = await img.metadata();

            switch (metadata.format) {
                case 'jpeg':
                    await Promise.all(
                        [
                            img.jpeg().toFile(`${filePath}/${fileName}`),
                            img.jpeg().resize(1920, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/lg-${fileName}`),
                            img.jpeg().resize(1280, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/md-${fileName}`),
                            img.jpeg().resize(768, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/sm-${fileName}`),
                        ],
                    );
                    break;
                case 'png':
                    await Promise.all(
                        [
                            img.png().toFile(`${filePath}/${fileName}`),
                            img.png().resize(1920, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/lg-${fileName}`),
                            img.png().resize(1280, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/md-${fileName}`),
                            img.png().resize(768, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${filePath}/sm-${fileName}`),
                        ],
                    );
                    break;
                default:
                    return Promise.reject(API_ERROR.BAD_REQUEST);
            }
            return Promise.resolve(fileName);
        } catch (err) {
            console.log(err);
            return Promise.reject(err);
        }
    }

    async uploadPrivate(file: any): Promise<string> {
        if (!file) return Promise.reject(API_ERROR.BAD_REQUEST);
        const filePath = PRIVATE_DIR_PATH;

        if (!fs.existsSync(filePath)) fs.mkdirSync(filePath);

        const id = uuid.v1();
        const fileName = slugify(`${id}_${file.name}`);
        await file.mv(`${filePath}/${fileName}`);
        return Promise.resolve(fileName);
    }

    // async uploadImg(image: any): Promise<string> {
    //     if (!image) return Promise.reject(API_ERROR.BAD_REQUEST);
    //     const imagePath = path.join(__dirname, '..', 'assets', 'public');

    //     if (!fs.existsSync(imagePath)) fs.mkdirSync(imagePath);

    //     try {
    //         const img = sharp(image.data);
    //         const imgPath = `${imagePath}`;
    //         let imgName;
    //         const metadata = await img.metadata();

    //         switch (metadata.format) {
    //             case 'jpeg':
    //                 imgName = `${uuid.v1()}.jpeg`;
    //                 await Promise.all(
    //                     [
    //                         img.jpeg().toFile(`${imgPath}/${imgName}`),
    //                         img.jpeg().resize(1920, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/lg-${imgName}`),
    //                         img.jpeg().resize(1280, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/md-${imgName}`),
    //                         img.jpeg().resize(768, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/sm-${imgName}`),
    //                     ],
    //                 );
    //                 break;
    //             case 'png':
    //                 imgName = `${uuid.v1()}.png`;
    //                 await Promise.all(
    //                     [
    //                         img.png().toFile(`${imgPath}/${imgName}`),
    //                         img.png().resize(1920, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/lg-${imgName}`),
    //                         img.png().resize(1280, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/md-${imgName}`),
    //                         img.png().resize(768, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/sm-${imgName}`),
    //                     ],
    //                 );
    //                 break;
    //             default:
    //                 return Promise.reject(API_ERROR.BAD_REQUEST);
    //         }
    //         return Promise.resolve(imgName);
    //     } catch (err) {
    //         console.log(err);
    //         return Promise.reject(err);
    //     }
    // }

    // async uploadAvatar(image: any): Promise<string> {
    //     if (!image) return Promise.reject(API_ERROR.BAD_REQUEST);
    //     const imagePath = path.join(__dirname, '..', 'assets', 'public');

    //     if (!fs.existsSync(imagePath)) fs.mkdirSync(imagePath);

    //     try {
    //         const img = sharp(image.data);
    //         const imgPath = `${imagePath}`;
    //         let imgName;
    //         const metadata = await img.metadata();

    //         switch (metadata.format) {
    //             case 'jpeg':
    //                 imgName = `${uuid.v1()}.jpeg`;
    //                 await Promise.all(
    //                     [
    //                         img.jpeg().resize(768, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/sm-${imgName}`),
    //                     ],
    //                 );
    //                 break;
    //             case 'png':
    //                 imgName = `${uuid.v1()}.png`;
    //                 await Promise.all(
    //                     [
    //                         img.png().resize(768, null,
    //                             {
    //                                 withoutEnlargement: true,
    //                                 kernel: sharp.kernel.lanczos3,
    //                             }).toFile(`${imgPath}/sm-${imgName}`),
    //                     ],
    //                 );
    //                 break;
    //             default:
    //                 return Promise.reject(API_ERROR.BAD_REQUEST);
    //         }
    //         return Promise.resolve(imgName);
    //     } catch (err) {
    //         console.log(err);
    //         return Promise.reject(err);
    //     }
    // }
}