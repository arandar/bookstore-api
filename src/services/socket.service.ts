import * as socketIo from 'socket.io';
import { Inject, Logger, SERVER_PROVIDER } from '@typespring/core';
import { ConfigService } from './config.service';
import { MongoService, Injectable } from '@typespring/core';
import { SocketEvent } from '../models/enums/socket-event';
import { Server } from 'http';
import * as winston from 'winston';

@Injectable()
export class SocketService {
    @Logger('SOCKET_SERVICE')
    private logger: winston.Logger;
    private io: socketIo.Server;

    constructor(
        private config: ConfigService,
        private mongo: MongoService,
        @Inject(SERVER_PROVIDER) private server: Server,
    ) {
    }

    init(): void {
        this.logger.info(`Socket.io is listening on http://localhost:${this.config.apiPort}`);
        this.io = socketIo(this.server, {
            serveClient: false,
        });
        this.io.sockets.on(SocketEvent.CONNECT, (socket) => {
            this.logger.debug('CLIENT CONNECTED');
            // this.socketConnection(socket);
        });
    }

    // socketConnection(socket): void {
    //     const roomId = socket.handshake.query.locationId;
    //     if (!roomId) {
    //         socket.disconnect(true);
    //         return;
    //     }
    //     this.connectRoom(socket, roomId);
    // }
    //
    // private connectRoom(socket, roomId): void {
    //     socket.on(SocketKey.E3, async (id: string) => {
    //         Object.keys(socket.rooms).forEach(el => socket.leave(el));
    //         socket.join(id);
    //         this.logger.info(`User connected to the room ${id}`);
    //     });
    //     socket.on(SocketKey.E7, async (data: ISocketData) => {
    //         this.setDialogAsRead(data);
    //     });
    //     socket.on(SocketKey.E2, () => {
    //         this.logger.info(`User disconnected`);
    //     });
    // }
}
