import * as cron from 'node-cron';
import * as path from 'path';
import * as fs from 'fs';
import * as winston from 'winston';
import { Collections } from '../models/enums/collections';
import { Injectable, MongoService,  Logger } from '@typespring/core';
import { EVERY_DAY } from '../models/const/cron';
import { PUBLIC_DIR_PATH } from '../models/const/paths';

@Injectable()
export class ImageCleanerService {
    @Logger('UPLOADER_SERVICE')
    private logger: winston.Logger;
    private cleanPeriod = EVERY_DAY;
    private PUBLIC_FOLDER = PUBLIC_DIR_PATH;

    constructor(
        private mongo: MongoService,
    ) {
        this.runImageCleaner();
    }

    runImageCleaner(): void {
        cron.schedule(this.cleanPeriod, async () => {
            this.logger.info(`Starting image cleaner service at ${new Date()}`);
            this.cleanImagesFolder();
        });
    }

    async cleanImagesFolder(): Promise<any> {
        const dirPath = `${this.PUBLIC_FOLDER}`;
        let images: string[];
        try {
            images = fs.readdirSync(dirPath);
        } catch (err) {
            this.logger.error(`Loading cleaning error: ${err}`);
            return;
        }
        if (!images.length) {
            this.logger.info('There are not images in folder...');
            return;
        }
        const collectionImages = await this.getImages();

        images.forEach(image => {
            if (image === '.gitkeep') return;
            const img = (
                image.indexOf('lg-') === 0 ||
                image.indexOf('md-') === 0 ||
                image.indexOf('sm-') === 0) ? `${image.substring(3)}` :
                `${image}`;
            if (!collectionImages.includes(img)) {
                console.log('Unused image found: ', image);
                try {
                    fs.unlinkSync(`${dirPath}/${image}`);
                } catch (error) {
                    this.logger.error(`Deleting unused public image error: ${error}.`);
                }
            }
        });
        this.logger.info('Deleting unused images success.');
    }

    async getImages(): Promise<string[]> {
        const img: any = await this.mongo.collection(Collections.Books).find().toArray();
        const imgFiles = img.map(i => i.img);
        return imgFiles;
    }
}