import { Injectable } from '@typespring/core';
import * as dotenv from 'dotenv';
import { ICredentials } from '../models/interfaces/credentials';
import {
    IMailerConfig,
    IMongoConfig,
} from '../models/interfaces/config';

dotenv.config();

@Injectable()
export class ConfigService {
    private _env: any = process.env;

    get production(): boolean {
        return this._convertToBoolean(this._env.PROD);
    }

    get apiPort(): string {
        return this._env.API_PORT;
    }

    get baseUrl(): string {
        return this._env.BASE_URL;
    }

    get apiUrl(): string {
        return this._env.API_URL;
    }

    get reviewUrl(): string {
        return this._env.REVIEW_URL;
    }

    get admin(): ICredentials {
        return {
            email: this._env.SUPERARMIN_EMAIL,
            password: this._env.SUPERADMIN_PASSWORD,
        };
    }

    get mailer(): IMailerConfig {
        return {
            email: this._env.MAILER_EMAIL,
            password: this._env.MAILER_PASSWORD,
            service: this._env.MAILER_SERVICE,
            host: this._env.MAILER_HOST,
            port: this._env.MAILER_PORT,
        };
    }

    get mongo(): IMongoConfig {
        return {
            url: this._env.MONGO_URL,
            dbName: this._env.MONGO_DB_NAME,
        };
    }

    private _convertToBoolean(input: string): boolean | undefined {
        try {
            return JSON.parse(input);
        } catch (e) {
            return undefined;
        }
    }
}
