import * as jwt from 'jsonwebtoken';
import * as fs from 'file-system';
import { Injectable } from '@typespring/core';
import { NextFunction, Request, Response } from 'express';
import { ITokenData } from '../models/interfaces/credentials';
import { Logger } from '@typespring/core';
import * as winston from 'winston';
import { KEYS_PATH } from '../models/const/paths';
import { UserRole } from '../models/enums/user-role';

@Injectable()
export class AccessGuardService {
    @Logger('ACCESS_GUARD')
    private logger: winston.Logger;

    static validateAdmin(req: Request, res: Response, next: NextFunction) {
        let token = req.headers.authorization;
        if (!token) {
            res.status(401).send({err: 'No token provided'});
            return;
        }
        token = token.replace('Bearer ', '');
        try {
            const publicKey = fs.readFileSync(`${KEYS_PATH}/key.pub`, 'utf8');
            const decoded = <ITokenData>jwt.verify(token, publicKey, {algorithms: ['RS256']});
            // Не уверен
            res.locals.tokenData = decoded;
            if (decoded.role !== UserRole.Superadmin) {
                res.status(401).send({err: 'Not authorized'});
            } else {
                next();
            }
        } catch (err) {
            res.status(401).send({err: err});
        }
    }

    static validateToken(req: Request, res: Response, next: NextFunction) {
        let token = req.headers.authorization;
        if (!token) {
            res.status(401).send({err: 'No token provided'});
            return;
        }
        token = token.replace('Bearer ', '');
        try {
            const publicKey = fs.readFileSync(`${KEYS_PATH}/key.pub`, 'utf8');
            const decoded = jwt.verify(token, publicKey, {algorithms: ['RS256']});
            res.locals.tokenData = decoded;
            next();
        } catch (err) {
            res.status(401).send({err: err});
        }
    }
}
