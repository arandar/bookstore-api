// import { Service } from 'typespring';
// import { IUser } from '../models/interfaces/user';
// import * as nodemailer from 'nodemailer';

// @Service()
// export class MailerService {
//     sendMail(user: IUser): void {
//         console.log(user._id);
//         const transporter = nodemailer.createTransport({
//             host: 'smtp.gmail.com',
//             port: 465,
//             secure: true,
//             auth: {
//                 type: 'OAuth2',
//                 clientId: '570703703725-5ofgldrou1ca64cj9jfi07kcvmcg2o4e.apps.googleusercontent.com',
//                 clientSecret: 'oBNGEzg43K9NmnP8Bm4USTDN',
//             },
//         });
//         console.log('created');
//         transporter.sendMail({
//             from: '"BookStore Service 📚" <bookstore.service.mailer@gmail.com>',
//             to: user.email,
//             subject: 'Confirm registration',
//             html: `<h3>*This message was generated automatically, please don't reply to it.</h3><br>
//             Thanks for registration on BookStore! Now the whole universe is opened for you. We love our customers
//             and give a lot of bonuses for registered users.<br>
//             There's still a little bit. Follow the link to
//             <a href="http://localhost:4200/confirm/${user._id}">confirm registration</a>.`,
//             auth: {
//                 user: 'bookstore.service.mailer@gmail.com',
//                 refreshToken: '1/RoNir7d7XU79U0jfl79L2vVZYsdKGe4UY9kJX9XK6kY',
//             },
//         });
//     }

//     resetPassword(user: IUser): void {
//         // console.log(user.email);
//         const transporter = nodemailer.createTransport({
//             service: 'Gmail',
//             host: 'smtp.gmail.com',
//             port: 465,
//             secure: true,
//             auth: {
//                 type: 'OAuth2',
//                 clientId: '570703703725-5ofgldrou1ca64cj9jfi07kcvmcg2o4e.apps.googleusercontent.com',
//                 clientSecret: 'oBNGEzg43K9NmnP8Bm4USTDN',
//             },
//         });

//         console.log('created');
//         transporter.sendMail({
//             from: '"BookStore Service 📚" <bookstore.service.mailer@gmail.com>',
//             to: user.email,
//             subject: 'Set new password',
//             html: `<h3>*This message was generated automatically, please don't reply to it.</h3><br>
//             Тут чё нить сам придумай)))
//             Follow the link to
//             <a href="http://localhost:4200/set-password/${user._id}">set new password</a>.`,
//             auth: {
//                 user: 'bookstore.service.mailer@gmail.com',
//                 refreshToken: '1/RoNir7d7XU79U0jfl79L2vVZYsdKGe4UY9kJX9XK6kY',
//             },
//         });
//     }
// }
import * as nodemailer from 'nodemailer';
import { ConfigService } from './config.service';
import { Injectable } from '@typespring/core';
import { Logger } from '@typespring/core';
import * as winston from 'winston';

@Injectable()
export class MailerService {
    @Logger('MAILER_SERVICE')
    private logger: winston.Logger;
    private transporter: nodemailer.Transporter;
    private mailOptions: nodemailer.SendMailOptions;

    constructor(
        private config: ConfigService,
    ) {
    }

    init() {
        this.initTransporter();
        this.initMailOptions();
    }

    // initTransporter(): void {
    //     const config = this.config.mailer;
    //     this.transporter = nodemailer.createTransport({
    //         host: config.host,
    //         port: config.port,
    //         secure: true,
    //         auth: {
    //             user: config.email,
    //             pass: config.password,
    //         },
    //     });
    // }

    // for the test mailer
    initTransporter(): void {
        this.transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'qa.ais2018@gmail.com',
                pass: 'XfHfxjrO',
            },
        });
    }

    initMailOptions(): void {
        const config = this.config.mailer;
        this.mailOptions = {
            list: {
                help: `${config.email}?subject=help`,
                unsubscribe: {
                    url: `mailto:${config.email}?subject=unsubscribe`,
                    comment: 'Unsubscribe',
                },
            },
            from: `QA AIS <${config.email}>`,
        };
    }

    send(recipient: string, subject: string, html: string, attachments?: any): Promise<void> {
        this.logger.debug(`Sending request to ${recipient}...`);
        return new Promise(async (resolve, reject) => {
            const mail = Object.assign({}, this.mailOptions);
            mail.to = recipient;
            mail.subject = subject;
            mail.html = html;
            if (attachments) {
                mail.attachments = [attachments];
            }
            try {
                const res = await this.transporter.sendMail(mail);
                if (res.error) {
                    this.logger.error(res.error);
                    reject(res.error);
                } else {
                    this.logger.debug(`Email request to user ${recipient} has been successfully sent.`);
                    resolve();
                }
            } catch (e) {
                this.logger.error(e);
                reject(e);
            }
        });
    }
}
