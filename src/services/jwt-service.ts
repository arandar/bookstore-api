import * as jwt from 'jsonwebtoken';
import * as fs from 'file-system';
import * as path from 'path';
import { ITokenData } from '../models/interfaces/credentials';
import { Injectable } from '@typespring/core';
import { Logger } from '@typespring/core';
import * as winston from 'winston';
import { KEYS_PATH } from '../models/const/paths';

@Injectable()
export class JWTService {
    @Logger('JWT_SERVICE')
    private logger: winston.Logger;
    private readonly algorithm = 'RS256';
    private readonly accessTokenExpiration = '2h';
    private readonly refreshTokenExpiration = '30d';

    getAccessToken(data: ITokenData): string {
        const privateKey = this.privateKey;
        return jwt.sign(data, privateKey, {
            algorithm: this.algorithm,
            expiresIn: this.accessTokenExpiration,
        });
    }

    getRefreshToken(data: ITokenData): string {
        const privateKey = this.privateKey;
        return jwt.sign(data, privateKey, {
            algorithm: this.algorithm,
            expiresIn: this.refreshTokenExpiration,
        });
    }

    private get privateKey(): string {
        return fs.readFileSync(path.join(KEYS_PATH, 'key.pem'), 'utf8');
    }
}
