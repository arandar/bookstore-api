import { Controller, Logger, PostRequest, GetRequest } from '@typespring/core';
import { ICredentials, ITokenData } from '../models/interfaces/credentials';
import { Request, Response } from 'express';
import { API_ERROR } from '../models/const/errors';
import * as winston from 'winston';
import { UsersRepository } from '../repository/users';
import { ConfigService } from '../services/config.service';
import { JWTService } from '../services/jwt-service';
import { UserRole } from '../models/enums/user-role';
// import * as nodemailer from 'nodemailer';
// import { MailerService } from '../services/mailer.service';
import { AccessGuardService } from '../services/access-guard.service';

@Controller('/auth')
export class AuthorizationController {
    @Logger('AUTH')
    private logger: winston.Logger;

    constructor(
        private config: ConfigService,
        // private authorizationService: AuthorizationService,
        private jwt: JWTService,
        // private authRepo: AuthorizationRepository,
        private usersRepo: UsersRepository,
    ) {
    }

    @PostRequest('/login')
    async login(req: Request): Promise<{ accessToken: string, refreshToken: string }> {
        const credentials: ICredentials = req.body;
        if (!credentials || !credentials.email || !credentials.password) return Promise.reject(API_ERROR.BAD_REQUEST);

        const adminCredentials: ICredentials = this.config.admin;
        if (credentials.email.toLowerCase() === adminCredentials.email.toLowerCase()
            && credentials.password === adminCredentials.password) {
            const accessToken = this.jwt.getAccessToken({ userId: '', role: UserRole.Superadmin });
            const refreshToken = this.jwt.getRefreshToken({ userId: '', role: UserRole.Superadmin });
            return { accessToken: accessToken, refreshToken: refreshToken };
        } else {
            const user = await this.usersRepo.getUserByCredentials(credentials);
            if (!user) return Promise.reject(API_ERROR.NOT_AUTHORIZED);

            const accessToken = this.jwt.getAccessToken({
                userId: user._id.toString(),
                role: user.role,
            });
            const refreshToken = this.jwt.getRefreshToken({
                userId: user._id.toString(),
                role: user.role,
            });

            return { accessToken: accessToken, refreshToken: refreshToken };
        };
    }

    @GetRequest('/refresh', [AccessGuardService.validateToken])
    async refreshAccessToken(req: Request, res: Response): Promise<string> {
        const tokenData: ITokenData = res.locals.tokenData;
        const user = await this.usersRepo.getUserById(tokenData.userId);
        if (!user) {
            return Promise.reject(API_ERROR.NOT_AUTHORIZED);
        }
        return this.jwt.getAccessToken({
            userId: user._id.toString(),
            role: user.role,
        });
    }


    // @PostRequest('/registration')
    // async regUser(req: Request): Promise<IUser> {
    //     const checkUser = await this.mongo.collection(Collections.Users).findOne({ username: req.body.username });
    //     if (checkUser) return Promise.reject(API_ERROR.CONFLICT);
    //     const check = req.body;
    //     check.active = false;
    //     const user = await this.authRepo.regUser(check);
    //     this.mailerService.sendMail(user);
    // }

    // @PostRequest('/confirm')
    // async confirmRegistration(req: Request): Promise<void> {
    //     console.log(req.body);
    //     return await this.authRepo.confirmRegistration(req.body.id);
    // }

    // @PostRequest('/login')
    // async getLogin(req: Request): Promise<any> {
    //     const credentials: ICredentials = req.body;
    //     if (!credentials) return Promise.reject(API_ERROR.BAD_REQUEST);
    //     this._logger.info(`Login: ${credentials.username}, Password: ${credentials.password}`);
    //     const accounts: IAccountsConfig = this.config.accounts;
    //     const adminAccount: ICredentials = accounts.admin;
    //     const userAccount = await this.mongo.collection(Collections.Users)
    //         .findOne({ username: credentials.username });
    //     console.log(credentials);

    //     if (credentials.username === adminAccount.username && credentials.password === adminAccount.password) {
    //         return await this.authorizationService.getToken(AccountType.Service);
    //     }

    //     if (credentials.username === userAccount.username
    //         && credentials.password === userAccount.password
    //         && userAccount.active === true) {
    //         return await this.authorizationService.getToken(AccountType.User, userAccount._id);
    //     }

    //     return Promise.reject(API_ERROR.NOT_AUTHORIZED);
    // }

    // @PostRequest('/get-profile')
    // async getProfile(req: Request): Promise<any> {
    //     const id = new ObjectId(req.body.id);
    //     this._logger.info(`getprofile from body: ${id}`);
    //     const user = await this.mongo.collection(Collections.Users).findOne({ _id: id });
    //     return {
    //         firstName: user.firstName,
    //         lastName: user.lastName,
    //         username: user.username,
    //         adress: user.adress,
    //         phone: user.phone,
    //         email: user.email,
    //         avatar: user.avatar,
    //     };
    // }
}