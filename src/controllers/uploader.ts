import { Request, Response } from 'express';
import { Controller, Logger, PostRequest, GetRequest, DeleteRequest } from '@typespring/core';
import { UploaderService } from '../services/uploader.service';
import * as path from 'path';
import * as winston from 'winston';
import { API_ERROR } from '../models/const/errors';
import { AccessGuardService } from '../services/access-guard.service';

@Controller('/uploader')
export class UploaderController {
    @Logger('UPLOADER')
    private logger: winston.Logger;

    constructor(
        private uploader: UploaderService,
    ) {
    }

    @PostRequest('/public', [AccessGuardService.validateToken])
    async uploadPublic(req: Request): Promise<string> {
        // console.log(req.files);
        return await this.uploader.uploadPublic(req.files.file);
    }

    @PostRequest('/private', [AccessGuardService.validateToken])
    async uploadPrivate(req: Request): Promise<string> {
        return await this.uploader.uploadPrivate(req.files.file);
    }

    // @PostRequest('/uploadAvatar')
    // async uploadAvatar(req: Request): Promise<string> {
    //     this._logger.info('Uploading files');
    //     return this.uploaderService.uploadAvatar(req.files.image);
    // }
}