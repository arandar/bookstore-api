import { Request } from 'express';
import { OrdersRepository } from './../repository/orders';
import { IOrder } from '../models/interfaces/order';
import { Controller, Logger, PostRequest, GetRequest, DeleteRequest } from '@typespring/core';
import * as winston from 'winston';
import { AccessGuardService } from '../services/access-guard.service';

@Controller('/order')
export class OrdersController {
    @Logger('AUTH')
    private logger: winston.Logger;

    constructor(
        private ordersRepo: OrdersRepository,
    ) {
    }

    @GetRequest('/',  [AccessGuardService.validateAdmin])
    async getOrders(req: Request): Promise<IOrder[]> {
        return await this.ordersRepo.getOrders();
    }

    @PostRequest('/')
    async addOrder(req: Request): Promise<IOrder> {
        return await this.ordersRepo.addOrder(req.body);
    }

    @DeleteRequest('/:id', [AccessGuardService.validateAdmin])
    async deleteBook(req: Request): Promise<void> {
        return await this. ordersRepo.deleteOrder(req.params.id);
    }
}