import { Request } from 'express';
import { Controller, Logger, PostRequest, GetRequest, DeleteRequest } from '@typespring/core';
import { UsersRepository } from '../repository/users';
import { IUser } from '../models/interfaces/user';
import * as winston from 'winston';

@Controller('/users')
export class UsersController {
    @Logger('USERS')
    private logger: winston.Logger;

    constructor(
        private usersRepo: UsersRepository,
    ) {
    }

    @GetRequest('/')
    async getUsers(req: Request): Promise<IUser[]> {
        return await this.usersRepo.getUsers();
    }

    // @PutRequest('/update')
    // async editUser(req: Request): Promise<void> {
    //     this._logger.info('Updating user');
    //     return await this.usersRepo.editUser(req.body);
    // }

    // @PostRequest('/reset-password')
    // async forgotPassword(req: Request): Promise<void> {
    //     this._logger.info('Reset user`s password');
    //     return await this.usersRepo.forgotPassword(req.body.email);
    // }

    // @PutRequest('/set-password')
    // async setNewPassword(req: Request): Promise<void> {
    //     this._logger.info('Set new user`s password');
    //     return await this.usersRepo.setNewPassword(req.body);
    // }

    // @PutRequest('/save-books')
    // async saveBooks(req: Request): Promise<void> {
    //     this._logger.info('Save users books');
    //     return await this.usersRepo.saveBooks(req.body.id, req.body.books);
    // }
}