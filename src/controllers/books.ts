import { Request } from 'express';
import { BooksRepository } from './../repository/books';
import { Controller, DeleteRequest, GetRequest, Logger, PostRequest, PutRequest } from '@typespring/core';
import { IBook } from '../models/interfaces/book';
import * as winston from 'winston';
import { AccessGuardService } from '../services/access-guard.service';

@Controller('/books')
export class BooksController {
    @Logger('BOOKS')
    private logger: winston.Logger;


    constructor(
        private booksRepo: BooksRepository,
    ) {
    }

    @PutRequest('/')
    async getAllBooks(req: Request): Promise<{books: IBook[], booksCount: number}> {
        return await this.booksRepo.getBooks(req.body.index, req.body.size, req.body.search);
    }

    @PostRequest('/add', [AccessGuardService.validateAdmin])
    async addBook(req: Request): Promise<IBook> {
        return await this.booksRepo.addBook(req.body);
    }

    @PutRequest('/update', [AccessGuardService.validateAdmin])
    async updateBook(req: Request): Promise<void> {
        return await this.booksRepo.updateBook(req.body);
    }

    @DeleteRequest('/:id', [AccessGuardService.validateAdmin])
    async deleteBook(req: Request): Promise<void> {
        return await this.booksRepo.deleteBook(req.params.id);
    }
}