export enum Collections {
    Books = 'books',
    Orders = 'orders',
    Users = 'users',
}