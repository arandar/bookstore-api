export enum UserRole {
    Superadmin = 'SUPERADMIN',
    Staff = 'STAFF',
    User = 'USER',
}