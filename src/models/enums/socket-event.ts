export enum SocketEvent {
    CONNECT = 'connection',
    DISCONNECT = 'disconnect',
    CHANGE_ROOM = 'change-room',
    ERROR = 'error',
}
