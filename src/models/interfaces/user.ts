import { ObjectId } from 'mongodb';
import { UserRole } from '../enums/user-role';
export interface IUser {
    _id?: ObjectId;
    role?: UserRole;
    firstName: string;
    lastName: string;
    username: string;
    email?: string;
    phone?: string;
    adress?: string;
    password: string;
    avatar: string;
    books?: ObjectId[];
}