import { ObjectId } from 'mongodb';
import { IPhone } from './phone';
export interface IOrder {
    _id?: ObjectId;
    name: string;
    phone?: IPhone;
    email?: string;
    adress: string;
    books: ObjectId[];
}