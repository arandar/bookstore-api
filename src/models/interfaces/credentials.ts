// import { AccountType, UserRole } from '../enums/account';
// import { ObjectId } from 'mongodb';

// export interface ICredentials {
//     username: string;
//     password: string;
// }

// export interface ITokenData {
//     accountType: AccountType;
//     sub: ObjectId;
// }

import { UserRole } from '../enums/user-role';

export interface ICredentials {
    email: string;
    password: string;
}

export interface ITokenData {
    role: UserRole;
    userId?: string;
}
