import { ObjectId } from 'mongodb';

export interface IBook {
    _id?: ObjectId;
    img: string;
    title: string;
    author: string;
    description: string;
    price: number;
}