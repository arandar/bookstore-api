export interface IMongoConfig {
    url: string;
    dbName: string;
}

export interface IMailerConfig {
    email: string;
    password: string;
    service: string;
    host: string;
    port: number;
}
