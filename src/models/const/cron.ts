export const EVERY_DAY = '0 0 * * *';
export const EVERY_FIVE_MINUTES = '*/5 * * * *';
export const EVERY_MINUTE = '* * * * *';
