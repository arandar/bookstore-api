import * as path from 'path';

export const ROOT = path.join(__dirname, '../', '../', '../');
export const STATIC_PATH = path.join(ROOT, 'static');
export const KEYS_PATH = path.join(ROOT, 'keys');
export const PUBLIC_DIR_PATH = path.join(ROOT, 'assets', 'public');
export const PRIVATE_DIR_PATH = path.join(ROOT, 'assets', 'private');
