# BS AIS-DREAM_TEAM API

## Development mode
Run `npm install` to install dependencies.
(OPTIONAL) Configure the project variables in ./environment/.env.dev.
(OPTIONAL) Run `npm run migrate:dev` to reset database and fill it with test data.
Run `npm run dev` to run the project and to watch file changes.

## Staging mode
Run `npm install` to install dependencies.
(OPTIONAL) Configure the project variables in ./environment/.stage.dev.
(OPTIONAL) Run `npm run migrate:stage` to reset database and fill it with test data.
Run `npm run stage` to run the project and to watch file changes.

## Production mode
Run `npm install` to install dependencies.
(OPTIONAL) Configure the project variables in ./environment/.prod.dev.
Run `npm run build:prod` to run the project.